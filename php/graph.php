<?php

require('database.php');

$sth = $DB->prepare('SELECT * FROM dt WHERE race_id = :race_id');
$sth->execute(array(':race_id'=>$_GET["race_id"]));
$results = $sth->fetchAll();


$dt;
$i = 0;
foreach ($results as $result) {
	$dt[$i] = $result['dt'];
	$i++;
}

$throttle;
$i = 0;
foreach ($results as $result) {
	$throttle[$i] = $result['throttle'] * 10;
	$i++;
}

require_once ('jpgraph/jpgraph.php');
require_once ('jpgraph/jpgraph_line.php');



// Setup the graph
$graph = new Graph(6400,1000);
$graph->SetScale("textlin");

$theme_class=new UniversalTheme;

$graph->SetTheme($theme_class);
$graph->img->SetAntiAliasing(false);
$graph->title->Set('Filled Y-grid');
$graph->SetBox(false);

$graph->img->SetAntiAliasing();

$graph->yaxis->HideZeroLabel();
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

$graph->xgrid->Show();
$graph->xgrid->SetLineStyle("solid");
$graph->xaxis->SetTickLabels(array('A','B','C','D'));
$graph->xgrid->SetColor('#E3E3E3');

// Create the first line
$p1 = new LinePlot($dt);
$graph->Add($p1);
$p1->SetColor("#6495ED");
$p1->SetLegend('Distance Traveled');

$p1 = new LinePlot($throttle);
$graph->Add($p1);
$p1->SetColor("#EA6764");
$p1->SetLegend('Throttle (×10)');

$graph->legend->SetFrameWeight(1);

// Output line
$graph->Stroke();

?>