<?php
require('database.php');



$sth = $DB->prepare('SELECT * FROM dt WHERE race_id = :race_id');
$sth->execute(array(':race_id'=>$_GET["race_id"]));
$results = $sth->fetchAll();

echo "<table border='1px'>";
?>

<tr>
	<th>
		id
	</th>
	<th>
		pipd
	</th>
	<th>
		ipd
	</th>
	<th>
		dt
	</th>
	<th>
		throttle
	</th>
	<th>
		lane
	</th>
</tr>

<?php
$prev_td = 0;
foreach ($results as $result) {
	echo '<tr>';
	echo '<td>';
	echo $result['id'];
	echo '</td>';
	echo '<td>';
	echo $result['pipd'];
	echo '</td>';
	echo '<td>';
	echo $result['ipd'];
	echo '</td>';
	if ($result['dt'] > ($prev_td + 1) || $result['dt'] < ($prev_td - 1)) {
		echo '<td style="background: red;">';
		echo $result['dt'];
		echo '</td>';
	}
	else if ($result['dt'] == 0) {
		echo '<td style="background: red;">';
		echo $result['dt'];
		echo '</td>';
	}
	else {
		echo '<td style="background: green;">';
		echo $result['dt'];
		echo '</td>';
	}
	echo '<td>';
	echo $result['throttle'];
	echo '</td>';
	echo '<td>';
	echo $result['lane'];
	echo '</td>';
	echo '</tr>';
	$prev_td = $result['dt'];
}

echo "</table>";

?>

