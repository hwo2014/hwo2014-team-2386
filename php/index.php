<?php
require('database.php');



$sth = $DB->prepare('SELECT * FROM races');
$sth->execute();
$results = $sth->fetchAll();

echo "<table border='1px'>";
?>

<tr>
	<th>
		race id
	</th>
	<th>
		race
	</th>
	<th>
		graph
	</th>
	<th>
		created
	</th>
</tr>

<?php

foreach ($results as $result) {
	echo '<tr>';
	echo '<td>';
	echo $result['race_id'];
	echo '</td>';
	echo '<td>';
	echo "<a href='race.php?race_id=" . $result['race_id'] . "'>" . "Race" . "</a>"; 
	echo '</td>';
	echo '<td>';
	echo "<a href='graph.php?race_id=" . $result['race_id'] . "'>" . "Graph" . "</a>"; 
	echo '</td>';
	echo '<td>';
	echo $result['created'];
	echo '</td>';
	echo '</tr>';
}
echo "</table>";

?>

