<?php
define('MAX_LINE_LENGTH', 1024 * 1024);

class BasicBot {
	protected $sock, $debug, $DB, $throttle, $start_throttle;
	public $track_pieces, $lanes, $pipd;
	
	function __construct($host, $port, $botname, $botkey, $debug = False) {
		$this->debug = $debug;


		/* Start Variables */
		
		$this->start_throttle = 0.65;

		/* End Vars */

		$this->connect($host, $port, $botkey);
		$this->write_msg('join', array(
			'name' => $botname,
			'key' => $botkey
			));
	}

	function __destruct() {
		if (isset($this->sock)) {
			socket_close($this->sock);
		}
	}

	protected function connect($host, $port, $botkey) {
		$this->sock = @ socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($this->sock === FALSE) {
			throw new Exception('socket: ' . socket_strerror(socket_last_error()));
		}
		if (@ !socket_connect($this->sock, $host, $port)) {
			throw new Exception($host . ': ' . $this->sockerror());
		}
	}

	protected function read_msg() {
		$line = @ socket_read($this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
		if ($line === FALSE) {
			$this->debug('** ' . $this->sockerror());
		} else {
			$this->debug('<= ' . rtrim($line));
		}
		return json_decode($line, TRUE);
	}

	protected function write_msg($msgtype, $data) {
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
		$this->debug('=> ' . rtrim($str));
		if (@ socket_write($this->sock, $str) === FALSE) {
			throw new Exception('write: ' . $this->sockerror());
		}
	}
	
	protected function sockerror() {
		return socket_strerror(socket_last_error($this->sock));
	}
	
	protected function debug($msg) {
		if ($this->debug) {
			echo $msg, "\n";
		}
	}



	public function run() {
		// Connect to the database
		$this->db_connect();
		/**
		*
		**/
		while (!is_null($msg = $this->read_msg())) {
			switch ($msg['msgType']) {

				case 'carPositions':
				$this->db_carPos($msg);

				$this->calculate_tick($msg);

				$this->write_msg('throttle', $this->throttle); // Sets the throttle -> Gets calculated in $this->calculate_tick
				break;
				case 'join':
				case 'yourCar':

				$this->write_msg('yourCar', array(
					'name' => 'Kingfire',
					'color' => 'black'
					));
				break;

				case 'gameInit':
				$this->lanes = $msg['data']['race']['track']['lanes'];
				$this->track_pieces = $msg['data']['race']['track']['pieces'];
				$this->calculate_corners();
				#print_r($this->track_pieces);
				#print_r(count($this->track_pieces));
				#print_r($this->track_pieces[4]);
				break;

				case 'gameStart':
					$this->write_msg('throttle', $this->start_throttle);
				break;
				case 'crash':
				case 'spawn':
				case 'lapFinished':
				case 'dnf':
				case 'finish':
				default:
				$this->write_msg('ping', null);
			}
		}
	}


	protected function db_connect() {

		$username = "hwo";
		$password = "hwo123";
		$host = "kingfire.org:3306";
		$dbname = "hwo";

		$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

		try
		{
			$this->DB = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password, $options);
		}
		catch(PDOException $ex)
		{
			die("Failed to connect to the database: " . $ex->getMessage());
		}

		$this->DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	}

	protected function db_carPos($data) {
		
		if (isset($data['gameTick'])) {

			$STH = $this->DB->prepare("INSERT INTO `carPositions` (gameid, tick, angle, throttle, pieceIndex, inPieceDistance, startLaneIndex, endLaneIndex, lap) VALUES (:gameid, :tick, :angle, :throttle, :pieceIndex, :inPieceDistance, :startLaneIndex, :endLaneIndex, :lap)"); 
			$STH->execute(array(':gameid'=>$data['gameId'],
				':tick'=>$data['gameTick'],
				':angle'=>$data['data'][0]['angle'],
				':throttle'=>$this->throttle,
				':pieceIndex'=>$data['data'][0]['piecePosition']['pieceIndex'],
				':inPieceDistance'=>$data['data'][0]['piecePosition']['inPieceDistance'],
				':startLaneIndex'=>$data['data'][0]['piecePosition']['lane']['startLaneIndex'],
				':endLaneIndex'=>$data['data'][0]['piecePosition']['lane']['endLaneIndex'],
				':lap'=>$data['data'][0]['piecePosition']['lap'],
				)); 
		}
	}

	protected function distance_traveled($pipd, $ipd, $dt, $current_lane) {
			$STH = $this->DB->prepare("INSERT INTO `dt` (pipd, ipd, dt, lane) VALUES (:pipd, :ipd, :dt, :lane)"); 
			$STH->execute(array(':pipd'=>$pipd,
				':ipd'=>$ipd,
				':dt'=>$dt,
				':lane'=>$current_lane
				)); 
	}


	/**
	* Calculates the length of the corners
	**/
	protected function calculate_corners() {

		foreach ($this->track_pieces as $key => $piece) {

			if (isset($piece['radius']) && isset($piece['angle'])) {
				foreach ($this->lanes as $laneKey => $lane) {
					if ($piece['angle'] < 0) {
						$diameter = ($piece['radius'] + $lane['distanceFromCenter']) * 2;
						$circumference = $diameter * pi();
						$length = ($circumference / 360) * abs($piece['angle']);
						$this->track_pieces[$key]['lanes'][$laneKey]['length'] = $length;
						$this->track_pieces[$key]['lanes'][$laneKey]['index'] = $lane['index'];
					}
					/**
					** Apperently Corners with negative angle are inverted? So lane 0 = higest | highest = 0
					**/
					else {
						$diameter = ($piece['radius'] + $lane['distanceFromCenter']) * 2;
						$circumference = $diameter * pi();
						$length = ($circumference / 360) * abs($piece['angle']);
						$this->track_pieces[$key]['lanes'][(count($this->lanes) -1) - $laneKey]['length'] = $length;
						$this->track_pieces[$key]['lanes'][(count($this->lanes) -1) - $laneKey]['index'] = $lane['index'];
					}
					#echo 'radius: ' . $piece['radius'] . ' distancefromcenter: ' . $lane['distanceFromCenter'] . ' angle: ' . $piece['angle'] . "\n" 
					#. 'diameter: ' . $diameter . ' circumference: ' . $circumference . ' length: ' . $length . "\n";
 				}
 				#exit;
			}

		}
	}

	protected function calculate_tick($data) {

		$ipd = $data['data'][0]['piecePosition']['inPieceDistance'];
		$pieceIndex = $data['data'][0]['piecePosition']['pieceIndex'];
		$current_lane = $data['data'][0]['piecePosition']['lane']['startLaneIndex'];
		$angle = $data['data'][0]['angle'];

		$dt = $this->calculate_distance($ipd, $pieceIndex, $current_lane); // Gets distance traveled

		
		if ($angle > 0) {
			$this->throttle = 0.80 - $angle;
		}
		else {
			$this->throttle = 0.80;
		}
		#echo $this->throttle . "\n";
		
	}

	protected function calculate_distance($ipd, $pieceIndex, $current_lane) {

		$dt = 0;

		/** 
		* If the car enterd a new piece
		**/
		if ($this->pipd > $ipd) { 
			/**
			* If the piece index is not 0
			* Get the length of the previous piece
			**/
			if ($pieceIndex != 0) {
				$current_piece = $this->track_pieces[$pieceIndex - 1]; // Gets the previous piece data
				if (isset($current_piece['radius'])) { // Checks if radius is set to see if it's a corner
					$length = $current_piece['lanes'][$current_lane]['length']; // Gets the lenght of the lane currently in
				}
				else {
					$length = $current_piece['length'];
				}
			}
			else {
				$nr_pieces = count($this->track_pieces);
				$current_piece = $this->track_pieces[$nr_pieces - 1];	

				if (isset($current_piece['radius'])) { // Checks if radius is set to see if it's a corner
					$length = $current_piece['lanes'][$current_lane]['length']; // Gets the lenght of the lane currently in
				}
				else {
					$length = $current_piece['length'];
				}
			}

			$dt = $ipd + ($length - $this->pipd);
		}
		/**
		* If the car is still on the same piece
		**/
		else {
			$dt = $ipd - $this->pipd;
		}

		

		$this->distance_traveled($this->pipd, $ipd, $dt, $current_lane);

		$this->pipd = $ipd; // Sets Previous In Piece Distance to the current one for next itteration
		#echo('PIPD: ' . $this->pipd . ' IPD: ' . $ipd. ' DT: ' . $dt . "\n");

		return $dt;
	}
}

?>