<?php

require('database.php');

$sth = $DB->prepare('SELECT * FROM dt');
$sth->execute();
$results = $sth->fetchAll();


$data;
$i = 0;
foreach ($results as $result) {
	$data[$i] = $result['dt'];
	$i++;
}

require_once ('jpgraph/jpgraph.php');
require_once ('jpgraph/jpgraph_line.php');



// Setup the graph
$graph = new Graph(1600,800);
$graph->SetScale("textlin");

$theme_class=new UniversalTheme;

$graph->SetTheme($theme_class);
$graph->img->SetAntiAliasing(false);
$graph->title->Set('Filled Y-grid');
$graph->SetBox(false);

$graph->img->SetAntiAliasing();

$graph->yaxis->HideZeroLabel();
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

$graph->xgrid->Show();
$graph->xgrid->SetLineStyle("solid");
$graph->xaxis->SetTickLabels(array('A','B','C','D'));
$graph->xgrid->SetColor('#E3E3E3');

// Create the first line
$p1 = new LinePlot($data);
$graph->Add($p1);
$p1->SetColor("#6495ED");
$p1->SetLegend('Line 1');

$graph->legend->SetFrameWeight(1);

// Output line
$graph->Stroke();

?>