<?php
require('database.php');

/*
$gameid = 'a036f413-ef21-4d99-970f-802b16b98328';

$sth = $DB->prepare('SELECT *
	FROM carPositions
	WHERE gameid = ?');
$sth->execute(array($gameid));
$results = $sth->fetchAll();


echo "<table border='1px'>";
?>

<tr>
	<th>
		Tick
	</th>
	<th>
		Angle
	</th>
	<th>
		Throttle
	</th>
	<th>
		pieceIndex
	</th>
	<th>
		inPieceDistance
	</th>
	<th>
		startLaneIndex
	</th>
	<th>
		endLaneIndex
	</th>
	<th>
		Lap
	</th>
</tr>

<?php
foreach ($results as $result) {
	echo '<tr>';
	echo '<td>';
	echo $result['tick'];
	echo '</td>';
	echo '<td>';
	echo $result['angle'];
	echo '</td>';
	echo '<td>';
	echo $result['throttle'];
	echo '</td>';
	echo '<td>';
	echo $result['pieceIndex'];
	echo '</td>';
	echo '<td>';
	echo $result['inPieceDistance'];
	echo '</td>';
	echo '<td>';
	echo $result['startLaneIndex'];
	echo '</td>';
	echo '<td>';
	echo $result['endLaneIndex'];
	echo '</td>';
	echo '<td>';
	echo $result['lap'];
	echo '</td>';
	echo '</tr>';
}
echo "</table>";
*/

$sth = $DB->prepare('SELECT * FROM dt');
$sth->execute();
$results = $sth->fetchAll();


echo "<table border='1px'>";
?>

<tr>
	<th>
		id
	</th>
	<th>
		pipd
	</th>
	<th>
		ipd
	</th>
	<th>
		dt
	</th>
</tr>

<?php
foreach ($results as $result) {
	echo '<tr>';
	echo '<td>';
	echo $result['id'];
	echo '</td>';
	echo '<td>';
	echo $result['pipd'];
	echo '</td>';
	echo '<td>';
	echo $result['ipd'];
	echo '</td>';
	echo '<td>';
	echo $result['dt'];
	echo '</td>';
	echo '</tr>';
}
echo "</table>";

?>